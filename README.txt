Description:
============
This module allows you to manipulate the links that are shown in the language switcher block
(as provided by the Locale module) and hide specified locales from the switcher block.

Use case:
You want to activate a new language on your website, but not advertise it yet
(e.g. you are still finishing up the translations). If you would leave the new language totally disabled,
you wouldn't be able to see the translated content in the proper interface.

Warning: This module just obfuscates the hidden languages.
Anyone who knows or finds the URL or language prefix, can still browse that part of the site.
So it should only be used for temporary workarounds. To have proper language access control,
there are probably better modules out there.

Installation:
=============
* Enable the module
* Go to admin/config/regional/language/locale_hide
* Select the languages you wish to hide from the language switcher block
