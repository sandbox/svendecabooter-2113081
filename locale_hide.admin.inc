<?php

/**
 * @file
 * Renders administrative pages for locale_hide module
 */

function locale_hide_admin_settings() {
  $form = array();

  $language_list = language_list('enabled');
  $lang_options = array();
  if ($language_list) {
    foreach ($language_list[1] as $langcode => $lang) {
      $lang_options[$langcode] = $lang->name;
    }
  }
  
  $form['locale_hide_languages'] = array(
    '#type' => 'checkboxes',
    '#title' => t('Languages to hide'),
    '#description' => t('Languages to hide from the language switcher'),
    '#options' => $lang_options,
    '#default_value' => variable_get('locale_hide_languages', array()),
  );

  return system_settings_form($form);
}